# Idena-node para 64bits

## Update imagen docker

##### En consola


ARM
```
export BUILD_VERSION_IDENA=0.30.0
docker build -t jerutyz/idena-node:ubuntu-arm64-${BUILD_VERSION_IDENA} .
docker push jerutyz/idena-node:ubuntu-arm64-${BUILD_VERSION_IDENA} 
```
Donde version igual version del nodo idena



## Validar desde la misma PC fisica

##### En consola

```
xhost +
```
access control disabled, clients can connect from any host
```
jmiremont@jeronimom-pc:~$ sudo su
root@jeronimom-pc:/home/jmiremont# su otro
otro@jeronimom-pc:/home/jmiremont$ idena-desktop
10.144.126.199 9010
```
Donde 'otro' es el otro usuario


oracle
http://10.144.126.199:9010
DELL
http://10.144.222.163:9010
DELL
http://10.144.222.163:9011
