## Installing operating system images on Linux

DOCUMENTATION > INSTALLATION > INSTALLING-IMAGES > LINUX

https://www.raspberrypi.org/documentation/installation/installing-images/linux.md

## SSH (Secure Shell)

DOCUMENTATION > REMOTE-ACCESS > SSH

```
sudo systemctl enable ssh
sudo systemctl start ssh
```


# Time Zone

```
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/America/Argentina/Mendoza /etc/localtime
```


# How to increase SWAP size in Raspberry Pi


Step 1: Stop the SWAP
```
sudo dphys-swapfile swapoff
```

Step 2: Modify the SWAP size
As root, edit the file /etc/dphys-swapfile and modify the variable CONF_SWAPSIZE:
```
sudo nano /etc/dphys-swapfile
```
Edit the line and enter decide swap size in MB
```
CONF_SWAPSIZE=1024
```

Step 3: Create and initialize the file
Run
```
sudo dphys-swapfile setup
```
Step 4: Start the SWAP
```
sudo dphys-swapfile swapon
```

https://diyusthad.com/2022/01/how-to-increase-swap-size-in-raspberry-pi.html

# Instalar zerotier

https://pimylifeup.com/raspberry-pi-zerotier/


## Installing ZeroTier to the Raspberry Pi

2. To install ZeroTier directly from their package repository to our Raspberry Pi, we will need to add the GPG key. This key helps verify the contents of the packages that we are installing are from ZeroTier.

Run the following command to download the GPG key from their GitHub repository, then save its “de-armored” contents into the “/usr/share/keyrings/” directory.


```
curl https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/zerotierone-archive-keyring.gpg >/dev/null
```

3. With the GPG key added, we need to create a source list that contains the ZeroTier repository.

Before we do that, we must first store the codename for the current operating system in a shell variable called “RELEASE“.

Running the following command will allow us to quickly build the correct URL for the ZeroTier repository in the next step.
```
RELEASE=$(lsb_release -cs)
```
4. We use the shell variable we set in the previous step to build the correct ZeroTier repository URL for your current operating system.

We then pipe this string into a file called “zerotier.list” that is stored within the “/etc/apt/sources.list.d/” directory.

```
echo "deb [signed-by=/usr/share/keyrings/zerotierone-archive-keyring.gpg] http://download.zerotier.com/debian/$RELEASE $RELEASE main" | sudo tee /etc/apt/sources.list.d/zerotier.list
```

When you update your Raspberry Pi’s package list again, it will read from this repository to find ZeroTier.

5. As we have made changes to our Raspberry Pi’s sources, we need to update the package list.

You can update the package list by running the following command on your system
```
sudo apt update
```

6. Once we have performed an update, we can finally install the ZeroTier package to our Raspberry Pi using the command below.
```
sudo apt install -y zerotier-one
```
As a part of the installation process, ZeroTier will automatically enable itself to start at boot.


## Running ZeroTier on the Raspberry Pi

Now that we have installed ZeroTier to our Raspberry Pi, we can connect it to the network we set up earlier in the guide.

Make sure that you have the ID of the network you want to connect to before continuing.

1. We will need to use the ZeroTier CLI to join the network on the Raspberry Pi.

To do this, you will need to use the following command. First, ensure that you replace “[NETWORKID]” with the ID you got earlier in this guide.
```
sudo zerotier-cli join [NETWORKID]
```
If your Raspberry Pi successfully joins the ZeroTier network, you should see the following message.

Response:

**200 join OK**


2. Even though you have joined the ZeroTier network, you are required to authenticate your device before it becomes an actual member of the network.

To do this, you will need to return to the ZeroTier Central interface and select your network.

If you want, you can also go to the following URL, replacing “[NETWORKID]” with your network ID.

https://my.zerotier.com/network/[NETWORKID]

And accept the new device


# Mount unidad externa
To mount ntfs devices
```
sudo apt install ntfs-3g

```

## Step 1 – Plug In The Device
The first step is to plug in your USB stick. If you are using a mouse and keyboard you will need a decent USB hub at this point. (e.g. the PiHub by Pimoroni).

```
ls -l /dev/disk/by-uuid/
```
## Step 2 – Identify The Devices Unique ID
In order to find the unique reference (UUID) for your drive run the following command in the terminal :


```
ls -l /dev/disk/by-uuid/
```
The line will usually refer to “/sda” and in this example it is “sda1”. My ID is “18A9-9943”. Note down yours.

You would need to repeat this step if you wanted to use a different device as the UUID would be different.

## Step 3 – Create a Mount Point
A mount point is a directory that will point to the contents of your flash drive. Create a suitable folder :


```
sudo mkdir /media/usb
```

I’m using “usb” but you can give it whatever name you like. Keep it short as it saves typing later on. Now we need to make sure the Pi user owns this folder :
```
sudo chown -R ${USER}:${USER} /media/usb
```
You will only need to do this step once.



## Step 5 – Un-mounting The Drive
You don’t need to manually un-mount if you shutdown your Pi but if you need to remove the drive at any other time you should un-mount it first. Only the user that mounted the drive can un-mount it.
```
umount /media/usb
```

If you used the fstab file to auto-mount it you will need to use :
```
sudo umount /media/usb
```
If you are paying attention you will notice the command is “umount” NOT “unmount”!


## Step 4 – Manually Mount The Drive
To manually mount the drive use the following command :
```
sudo mount /dev/sda1 /media/usb -o uid=pi,gid=pi
```
This will mount the drive so that the ordinary Pi user can write to it. Omitting the “-o uid=pi,gid=pi” would mean you could only write to it using “sudo”.

Now you can read, write and delete files using “/media/usb” as a destination or source without needing to use sudo.


## Step 6 – Auto Mount
When you restart your Pi your mounts will be lost and you will need to repeat Step 4. If you want your USB drive to be mounted when the system starts you can edit the fstab file :
```
sudo vim /etc/fstab
```
Then add the following line at the end :
```
UUID=18A9-9943 /media/usb ntfs auto,nofail,noatime,users,rw,uid=[USER],gid=[USER] 0 0
```
The “nofail” option allows the boot process to proceed if the drive is not plugged in. The “noatime” option stops the file access time being updated every time a file is read from the USB stick. This helps improve performance.

My fstab file looks like this :

