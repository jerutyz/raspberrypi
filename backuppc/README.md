Install backuppc
code
`$ sudo apt install samba-common samba smbclient libfile-rsyncp-perl backuppc`

`sudo vim /etc/apache2/conf-enabled/backuppc.conf`


```
       <RequireAll>
                # Comment out this line once you have setup HTTPS and uncommented SSLRequireSSL
                #Require local

                # This line ensures that only authenticated users may access your backups
                Require valid-user
        </RequireAll>
```


`htpasswd -c /etc/backuppc/htpasswd backuppc`


```
$ sudo systemctl backuppc restart
$ sudo systemctl apache2 restart
```

`sudo apt install      ntfs-3g`

```
# usamos la terminal como root porque vamos a ejecutar algunos comandos que necesitan ese modo de ejecución
sudo su
# buscamos el disco que querramos montar (por ejemplo la partición sdb1 del disco sdb)
fdisk -l
# pueden usar el siguiente comando para obtener el UUID
ls -l /dev/disk/by-uuid/
# y simplemente montamos el disco en el archivo /etc/fstab (pueden hacerlo por el editor que les guste o por consola)
echo UUID="{nombre del disco o UUID que es único por cada disco}" {directorio donde queremos montarlo} (por ejemplo /mnt/storage) ntfs-3g defaults,auto 0 0 | \
     sudo tee /etc/fstab
# por último para que lea el archivo fstab
mount -a (o reiniciar)
```



sudo -i
service backuppc stop
cd /var/lib
mkdir bpc.bak
cp -ar backuppc/* bpc.bak/
touch backuppc/USB_DRIVE_NOT_MOUNTED

mount -a

cp -ar bpc.bak/* backuppc/
touch backuppc/USB_DRIVE_MOUNTED
chown backuppc:backuppc /var/lib/backuppc
systemctl start backuppc







Fuentes, and thanks

https://no-sheds.blogspot.com/2017/05/setting-up-backuppc-on-raspberry-pi.html

https://monotai.com/howto-install-backuppc-on-a-raspberry-pi.html

