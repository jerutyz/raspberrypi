# Idena-node para 64bits

## Update imagen docker

##### En consola

x86-64
```
export BUILD_VERSION_IDENA=0.29.2
docker build -t jerutyz/idena-node:alpine64-${BUILD_VERSION_IDENA} .
docker push jerutyz/idena-node:alpine64-${BUILD_VERSION_IDENA} 
```
Donde version igual version del nodo idena


## Validar desde la misma PC fisica

##### En consola

```
xhost +
```
access control disabled, clients can connect from any host
```
jmiremont@jeronimom-pc:~$ sudo su
root@jeronimom-pc:/home/jmiremont# su otro
otro@jeronimom-pc:/home/jmiremont$ idena-desktop
```
Donde 'otro' es el otro usuario

